defmodule FireSave.Application do
  use Application

  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    children = [
      supervisor(FireSave.Repo, []),
    ]

    opts = [strategy: :one_for_one, name: FireSave.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
