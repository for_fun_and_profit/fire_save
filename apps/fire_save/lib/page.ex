defmodule FireSave.Page do
  use Ecto.Schema

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id

  schema "pages" do
    field :ref, :integer, read_after_writes: true
    field :url, :string
    field :title, :string

    timestamps()
  end
end
