defmodule FireSave do
  @moduledoc """
  Main module containing base `FireSave` functionality.
  """

  alias FireSave.{Page, Repo}
  alias Ecto.Changeset

  @doc """
  Scrapes web page the `url` points to.

  ## Examples:

      iex> FireSave.scrape("http://foo.com")
      {:ok, %{url: "http://foo.com", title: "Foo Page"}}

      iex> FireSave.scrape("http://non-existent-page.com")
      {:error, :page_not_found}

      iex> FireSave.scrape("this won't work")
      {:error, :invalid_url}

  """
  def scrape(url) do
    with true <- url_valid?(url),
         {:ok, url, body} <- get_url(url),
         {:ok, title} <- find_title(body) do
      {:ok, %{url: url, title: title}}
    else
      false  -> {:error, :invalid_url} # corresponds to failing `url_valid?`
      result -> result
    end
  end

  @doc """
  It creates `FireSave.Page` based on `params` passed.

  If `params` passed are valid - new `FireSave.Page` will be created, otherwise
  `changeset` with errors will be returned.

  ## Examples:

      iex> FireSave.add_page(%{url: "http://www.google.com", title: "Google"})
      {:ok, %FireSave.Page{...}}

      iex> FireSave.add_page(%{url: nil})
      {:error, %Ecto.Changeset{...}}

  """
  def add_page(params) do
    %Page{}
    |> Changeset.cast(params, [:url, :title])
    |> Changeset.validate_required([:url, :title])
    |> Changeset.validate_change(:url, fn(_, url) ->
         if url_valid?(url) do
           []
         else
           [url: "is invalid"]
         end
       end)
    |> Repo.insert()
  end

  ###
  # Private functions

  defp url_valid?(url) do
    url
    |> String.to_char_list()
    |> :http_uri.parse()
    |> case do
         {:ok, _} -> true
         _        -> false
       end
  end

  defp get_url(url) do
    case HTTPoison.get(url) do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        {:ok, url, body}

      {:ok, %HTTPoison.Response{status_code: code, headers: headers}}
      when code >= 300 and code < 400 ->
        headers
        |> Enum.find(fn({key, _}) -> String.downcase(key) == "location" end)
        |> elem(1)
        |> get_url()

      {:ok, %HTTPoison.Response{status_code: 404}} ->
        {:error, :page_not_found}

      {:error, %HTTPoison.Error{reason: :nxdomain}} ->
        {:error, :page_not_found}

      _ ->
        {:error, :internal_error_on_page}
    end
  end

  defp find_title(body) do
    title =
      body
      |> Floki.find("head title")
      |> Floki.text()

    {:ok, title}
  end
end
