# FireSave

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add `fire_save` to your list of dependencies in `mix.exs`:

    ```elixir
    def deps do
      [{:fire_save, "~> 0.1.0"}]
    end
    ```

  2. Ensure `fire_save` is started before your application:

    ```elixir
    def application do
      [applications: [:fire_save]]
    end
    ```

