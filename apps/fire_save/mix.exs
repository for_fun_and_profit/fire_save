defmodule FireSave.Mixfile do
  use Mix.Project

  def project do
    [app: :fire_save,
     version: "0.1.0",
     build_path: "../../_build",
     config_path: "../../config/config.exs",
     deps_path: "../../deps",
     lockfile: "../../mix.lock",
     elixir: "~> 1.3",
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     deps: deps(),
     aliases: aliases()]
  end

  def application do
    [applications: [:logger, :ecto, :postgrex, :httpoison],
     mod: {FireSave.Application, []}]
  end

  defp deps do
    [{:ecto, "~> 2.0"},
     {:postgrex, "~> 0.11"},
     {:floki, "~> 0.12.0"},
     {:httpoison, "~> 0.10.0"},
     {:poison, "~> 3.0"},

     # Testing
     {:bypass, "~> 0.5.1", only: :test}]
  end

  defp aliases do
    ["ecto.setup": ["ecto.create", "ecto.migrate", "run priv/repo/seeds.exs"],
     "ecto.reset": ["ecto.drop", "ecto.setup"],
     "test": ["ecto.create --quiet", "ecto.migrate --quiet", "test"]]
  end
end
