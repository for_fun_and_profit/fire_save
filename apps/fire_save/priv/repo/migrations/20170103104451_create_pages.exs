defmodule FireSave.Repo.Migrations.CreatePages do
  use Ecto.Migration

  def up do
    create table(:pages, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :ref, :"BIGSERIAL"
      add :url, :string
      add :title, :string

      timestamps()
    end

    execute("ALTER SEQUENCE pages_ref_seq START with 1000 RESTART")
    create unique_index(:pages, [:ref])
  end

  def down do
    drop unique_index(:pages, [:ref])
    drop table(:pages)
  end
end
