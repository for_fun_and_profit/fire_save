ExUnit.start()

Ecto.Adapters.SQL.Sandbox.mode(FireSave.Repo, :manual)
Application.ensure_all_started(:bypass)
