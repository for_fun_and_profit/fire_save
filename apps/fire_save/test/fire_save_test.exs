defmodule FireSaveTest do
  use ExUnit.Case, async: true

  setup tags do
    :ok = Ecto.Adapters.SQL.Sandbox.checkout(FireSave.Repo)

    unless tags[:async] do
      Ecto.Adapters.SQL.Sandbox.mode(FireSave.Repo, {:shared, self()})
    end

    bypass = Bypass.open()

    {:ok, bypass: bypass}
  end

  describe "FireSave.add_page/1" do
    test "when params are valid, saves the page and assigns ref number" do
      params = %{url: "http://google.com", title: "Google"}

      {:ok, google_page} = FireSave.add_page(params)

      assert google_page.url == "http://google.com"
      assert google_page.title == "Google"
      refute google_page.ref == nil
    end

    test "when params are invalid, returns changeset" do
      params = %{url: "", title: nil}
      {:error, changeset} = FireSave.add_page(params)
      assert {:url, {"can't be blank", validation: :required}} in changeset.errors
      assert {:title, {"can't be blank", validation: :required}} in changeset.errors

      params = %{url: "invalid string", title: "Testing!"}
      {:error, changeset} = FireSave.add_page(params)
      assert {:url, {"is invalid", []}} in changeset.errors
    end
  end

  describe "FireSave.scrape/1" do
    test "when url points to webpage", %{bypass: bypass} do
      Bypass.expect bypass, fn(conn) ->
        assert "GET" == conn.method
        Plug.Conn.resp(conn, 200, """
        <html>
            <head>
                <title>Example Page</title>
            </head>
        </html>
        """)
      end
      url = endpoint_url(bypass.port)

      {:ok, page} = FireSave.scrape(url)

      assert page.url == url
      assert page.title == "Example Page"
    end

    test "when url points to webpage, but it doesn't have title tag", %{bypass: bypass} do
      Bypass.expect bypass, fn(conn) ->
        assert "GET" == conn.method
        Plug.Conn.resp(conn, 200, """
        <html>
            <head>
            </head>
        </html>
        """)
      end
      url = endpoint_url(bypass.port)

      {:ok, page} = FireSave.scrape(url)

      assert page[:title] == ""
      assert page[:url] == url
    end

    test "follows redirect and sets proper url", %{bypass: bypass} do
      Bypass.expect bypass, fn(conn) ->
        case conn.request_path do
          "/to-redirect" ->
            conn
            |> Plug.Conn.put_resp_header("Location", endpoint_url(bypass.port, "redirected"))
            |> Plug.Conn.resp(302, "")

          "/redirected" ->
            Plug.Conn.resp(conn, 200, """
            <html>
                <head>
                    <title>Page after redirect</title>
                </head>
            </html>
            """)

          unrecognised_request_path ->
            raise("Unrecognised request_path: #{unrecognised_request_path}")
        end
      end
      url = endpoint_url(bypass.port, "to-redirect")

      {:ok, page} = FireSave.scrape(url)

      assert page.url == endpoint_url(bypass.port, "redirected")
      assert page.title == "Page after redirect"
    end

    test "when url points to non-existent page", %{bypass: bypass} do
      Bypass.expect bypass, fn(conn) ->
        Plug.Conn.resp(conn, 404, "")
      end
      url = endpoint_url(bypass.port)

      result = FireSave.scrape(url)

      assert {:error, :page_not_found} == result
    end

    test "page that url points to can't process request", %{bypass: bypass} do
      Bypass.expect bypass, fn(conn) ->
        Plug.Conn.resp(conn, 500, "")
      end
      url = endpoint_url(bypass.port)

      result = FireSave.scrape(url)

      assert {:error, :internal_error_on_page} == result
    end

    test "url is invalid" do
      url = "invalid string"

      result = FireSave.scrape(url)

      assert {:error, :invalid_url} == result
    end
  end

  defp endpoint_url(port, path \\ "") do
    "http://localhost:#{port}/#{path}"
  end
end
