use Mix.Config

config :logger, level: :warn

config :fire_save, FireSave.Repo,
  adapter: Ecto.Adapters.Postgres,
  database: "fire_save_test",
  username: "postgres",
  password: "postgres",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox
