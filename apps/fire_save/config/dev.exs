use Mix.Config

config :fire_save, FireSave.Repo,
  adapter: Ecto.Adapters.Postgres,
  database: "fire_save_dev",
  username: "postgres",
  password: "postgres",
  hostname: "localhost",
  pool_size: 10

import_config "dev.secret.exs"
